<?php
    namespace src\CritoCurrency;
    use src\Util\HttpRequest;

    class CritoCurrency {
        public $symbol;
        public $name;
        public $id;
        public $currencyToConvert;
        public $minToBuy;
        public $maxToSell;
        public $price;
        public $enableNotifyToSell;
        public $enableNotifyToBuy;

        public function __construct($symbol, $currencyToConvert, $min, $max, $notifyToSell, $notifyToBuy){
            $this->symbol = strtolower($symbol);
            $this->currencyToConvert = strtolower($currencyToConvert);
            $this->minToBuy = $min;
            $this->maxToSell = $max;
            $this->enableNotifyToSell = $notifyToSell;
            $this->enableNotifyToBuy = $notifyToBuy;
            $this->getDetail();
        }

        public function getDetail(){
            $string = file_get_contents("listCoins.json");
            $json_a = json_decode($string);
            for ($x = 0; $x < count($json_a); $x++) {
                if($json_a[$x]->symbol === $this->symbol) {
                    $this->name = $json_a[$x]->name;
                    $this->id = $json_a[$x]->id;
                    break;
                }
            }
        }

        public function getPrice($url){
            $curl = new HttpRequest($url);
            $dataParams = array (
                'ids' => $this->id,
                'vs_currencies' => $this->currencyToConvert
            );
            $api = "/simple/price/";
            $respond = $curl->getRequest($api, $dataParams);
            if(!empty($respond)) {
                $this->storePrice(json_decode($respond, true));
            }
        }

        public function storePrice($respond){
            $currency = $respond[$this->id];
            $this->price = $currency[$this->currencyToConvert];
        }

        public function analize(){
            $action = '';
            if($this->price <= $this->minToBuy && $this->enableNotifyToBuy === true) {
                $action = 'buy';
            } else if($this->price >= $this->maxToSell && $this->enableNotifyToSell === true) {
                $action = 'sell';
            } else {
                $action = 'nothing';
            }
            return $action;
        }
    }
?>
