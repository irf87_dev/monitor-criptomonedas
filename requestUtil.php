<?php
    namespace src\Util;
    class HttpRequest {
        private $host;

        function __construct($host) {
            $this->host = $host;
        }

        function getRequest($api, $dataArray = []) {
            //EXAMPLE http://agichevski.com/2014/01/21/php-curl-post-and-get-methods/
            $url = $this->host.$api;
            $curl = curl_init($url);
            $params = '';
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json'));

            if(!empty($dataArray) && count($dataArray) > 0) {
                foreach($dataArray as $key => $val) {
                    $params .= $key.'='.$val.'&';
                }
                $params = trim($params, '&');
                curl_setopt($curl, CURLOPT_URL, $url.'?'.$params ); //Url together with parameters
            }

            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT , 10); //Timeout after 7 seconds
            curl_setopt($curl, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);

            $result = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if($err) {
                return $err;
            }
            return $result;
        }

        function postRequest($api, $dataArray = []) {
            //EXAMPLE http://agichevski.com/2014/01/21/php-curl-post-and-get-methods/
            $url = $this->host.$api;
            $curl = curl_init($url);
            $params = '';
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                
            ));

            if(!empty($dataArray) && count($dataArray) > 0) {
                foreach($dataArray as $key => $val) {
                    $params .= $key.'='.$val.'&';
                }
                $params = trim($params, '&');
            }

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT , 10); //Timeout after 7 seconds
            curl_setopt($curl, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);

            curl_setopt($curl, CURLOPT_POST, count($dataArray)); //number of parameters sent
            print_r($params);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params); //parameters data

            $result = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if($err) {
                return $err;
            }
            return $result;
        }
    }
?>
