<?php
    namespace src\main;
    include "./telegramBot.php";
    include "./requestUtil.php";
    include "./analyzeCripto.php";
    include "./constants.php";
    use src\CritoCurrency\CritoCurrency;
    use src\Telegram\TelegramBot;
    
    $telegramCredentials = file_get_contents("telegramBot.json");
    $credentialsJson = json_decode($telegramCredentials);

    $string = file_get_contents("criptoIndicators.json");
    $json_a = json_decode($string);
    for ($x = 0; $x < count($json_a); $x++) {
        $crypto = new CritoCurrency(
            $json_a[$x]->critoCurrency,
            $json_a[$x]->currencyToConvert,
            $json_a[$x]->minToBuy,
            $json_a[$x]->maxToSell,
            $json_a[$x]->notifyMeToSell,
            $json_a[$x]->notifyMeToBuy
        );
        $crypto->getPrice(END_POINT);
        $actionToDo = $crypto->analize();

        $telegramBot = new TelegramBot(
            $credentialsJson->botId,
            $credentialsJson->botHash,
            $credentialsJson->chatId
        );

        $msg = $crypto->id." Acción: ".$actionToDo." Precio: ".$crypto->price.$crypto->currencyToConvert;
        if($actionToDo != "nothing") {
            $telegramBot->sendMessage($msg);
        }
    }
?>
