<?php
	//Info https://blog.meer-web.nl/sending-telegram-messages-using-php/
	namespace src\Telegram;
    use src\Util\HttpRequest;

	class TelegramBot {
		public $telegramBot;
		public $chatId;

		public function __construct($botId, $botHash, $chatId){
			$this->telegramBot = $botId.":".$botHash;
			$this->chatId = $chatId;
		}

		public function sendMessage($msg){
			$curl = new HttpRequest('https://api.telegram.org/');
			$dataParams = array (
                'chat_id' => $this->chatId,
                'text' => $msg
            );
            $api = 'bot'.$this->telegramBot.'/sendMessage';

            $curl->postRequest($api, $dataParams);
		}
	}

?>